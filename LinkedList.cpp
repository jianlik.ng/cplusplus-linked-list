
//implementation here

#include "LinkedList.h"
#include <iostream>

//Constructor
LinkedList::LinkedList() {
	head = nullptr;
	tail = nullptr;
	length = 0;
}

//Overloaded constructor
LinkedList::LinkedList(int initialData) {
	head = new Node(initialData, nullptr);
	tail = head;
	length = 1;
}


//Gets a node at a certain position
//Returns nullptr if not found (pos < 0, pos > length or (pos == 0 and currNode is null))
//Returns pointer to the node if found
Node* LinkedList::GetNodeAtPosition(int pos) {
	
	//return nullptr if node position does not make sense
	if (pos < 0) {
		return nullptr;
	}

	Node* currNode = head; //if head == nullptr (meaning no data, function should still work)
	
	//loop through to try find data at position
	for (int i = 0; i < pos; i++)
	{
		if (currNode == tail) return nullptr; //not found
		currNode = currNode->next;
	}
	return currNode;
}

//Inserts a new node at a specified position (has to be a node already at the existing position, except for position 0)
//Can handle length of 0
//Returns -1 if not possible
//Returns data inserted if successful
int LinkedList::InsertAtPosition(int data, int pos) {

	//If position is negative or if the position is greater than the LinkedList length
	if (pos < 0) {
		return -1; //invalid position to insert at
	}
	else if (pos == 0) { //this is the head
		return InsertAtHead(data);
	}
	else if (pos > length){
		return -1; //invalid position to insert at
	}
	else if (pos == length) { //this is the tail
		return InsertAtTail(data);
	}
	else {
		Node* prevNode = GetNodeAtPosition(pos - 1);
		Node* nextNode = prevNode->next;
		Node* newNode = new Node(data, nextNode);
		newNode->next = nextNode;
		prevNode->next = newNode;
		length++;

		return data;
	}

}

//Inserts a new node at the head of the Linked List
//Can handle length of 0
//Returns data inserted if successful
int LinkedList::InsertAtHead(int data) {
	//create the node
	Node* newNode = new Node(data, head);

	//if this is the first node
	if (length == 0) {
		tail = newNode;
	}
	//change the head pointer
	newNode->next = head;	//if head is nullptr (creating the very first node in the Linked List, this is still okay)
	head = newNode;
	length++;

	return data;
}

//Inserts a new node at the tail
//Can handle length of 0
//Returns data inserted if successful
int LinkedList::InsertAtTail(int data) {
	//create the new node
	Node* newNode = new Node(data);

	//if this is the first node
	if (length == 0) {
		head = newNode;
	}
	else {
		tail->next = newNode;
	}
	
	//change tail pointer to point to the new end
	tail = newNode;
	length++;

	return data;
}

//Sets the data for a node at a specified position
//Returns -1 if not possible
//Returns data that was set if successful
int LinkedList::SetDataAtPosition(int dataToSet, int pos) {

	Node* currNode = GetNodeAtPosition(pos);

	if (currNode == nullptr){
		return -1; //not found
	}
	else {
		currNode->data = dataToSet;
		return dataToSet; //success
	}
}


//Gets the data for a node at a specified position
//Returns -1 if not possible
//Returns data if successful
int LinkedList::GetDataAtPosition(int pos) {

	Node* currNode = GetNodeAtPosition(pos);

	if (currNode == nullptr) {
		return -1; //not found
	}
	else {
		return currNode->data;
	}
}

//Gets the data at the head
//Returns -1 if not possible
//Returns data at head if possible
int LinkedList::GetDataAtHead() {

	if (length == 0) { //if no data in linked list
		return -1;
	}
	else {
		return head->data;
	}
}

//Gets the data at the tail
//Returns -1 if not possible
//Returns data at tail if possible
int LinkedList::GetDataAtTail() {

	if (length == 0) { //if no data in linked list
		return -1;
	}
	else {
		return tail->data;
	}
}

//----------------------------------------------------------
//Print all node data
void LinkedList::PrintAllData() {

	Node* currNode = head;
	
	if (length == 0) {
		std::cout << "\nLinked List is empty!\n";
	}

	while (currNode != nullptr) {
		std::cout << currNode->data << ", ";
		currNode = currNode->next;
	} 
}


//Deletes a node at a certain position
//Returns the deleted data if node found
//Returns -1 if the data is not found
int LinkedList::DeleteAtPosition(int pos) {

	int dataToReturn = -1;

	// If position is negative or if the position is greater than the LinkedList length
	if (pos < 0) {
		//invalid position to delete at, do nothing
	}
	else if (pos == 0) { //this is the head
		dataToReturn = DeleteAtHead();
	}
	else if (pos >= length) {
		//invalid position to delete at, do nothing
	}
	else if (pos == length - 1) { //this is the tail
		dataToReturn = DeleteAtTail();
	}
	else {
		Node* prevNode = GetNodeAtPosition(pos - 1);
		Node* currNode = prevNode->next;
		Node* nextNode = currNode->next;
		prevNode->next = nextNode;
		dataToReturn = currNode->data;
		delete currNode;
		length--;
	}

	return dataToReturn;
}


//Deletes a node at the head
//Returns the deleted data if node found
//Returns -1 if the data is not found
int LinkedList::DeleteAtHead() {

	int dataToReturn = -1;
	//if Linked List is empty
	if (length == 0) {
		//do nothing
	}
	else if (length == 1) {
		dataToReturn = head->data;
		delete head;
		head = nullptr;
		tail = nullptr;
		length--;
	}
	else{
		dataToReturn = head->data;
		Node* originalHead = head;
		head = head->next;
		delete originalHead;
		length--;
	}
	return dataToReturn;
}


//Deletes a node at the tail
//Returns the deleted data if node found
//Returns -1 if the data is not found
int LinkedList::DeleteAtTail() {

	int dataToReturn = -1;

	//if Linked List is empty
	if (length == 0) {
		//do nothing
	}
	else if (length == 1) {
		dataToReturn = head->data;
		delete head;
		head = nullptr;
		tail = nullptr;
		length--;
	}
	else {
		dataToReturn = tail->data;
		Node* originalTail = tail;
		Node* penultimateNode = GetNodeAtPosition(length-2);
		penultimateNode->next = nullptr;
		tail = penultimateNode;
		delete originalTail;
		length--;
	}
	return dataToReturn;
}

LinkedList::~LinkedList() {
	if (head != nullptr) {
		delete head;
		head = nullptr;
	}
	if (tail != nullptr){
		delete tail;
		tail = nullptr;
	}
}
