/* Node class for hodling positive integer values
Author: Jian Lik Ng
Date: 2021/09/06
*/


#pragma once

class Node
{
	public:
		int data;
		Node* next; //pointer to the next node

		Node(); //constructor
		Node(int); //overload for constructor
		Node(int, Node*); //overload for constructor
		~Node(); //destructor
};

