// LinkedList.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "LinkedList.h"

//function declaration here (if put function declaration after main, then we need a function definition as well)
//Parameters: the variable to be set by the user input
//Returns: an integer set with the user input
int getUserIntInput(int setThisValue) {

    while (!(std::cin >> setThisValue)) {  //std::cin >> variable is a function. It will return false if it can't assign the input to the variable type
        std::cout << "\nInvalid input! Please enter in an integer value\n";
        std::cin.clear(); //clear the error flag on cin
        std::cin.ignore(256, '\n'); //Ignore previous stream up to 256 characters until '\n' is found
        //std::cin >> setThisValue;
    }

    return setThisValue;
}

int main()
{

    bool cont = true;
    int option = 0;
    int res = 0;
    int dataToIns = 0;
    int pos = 0;
    LinkedList myLinkedList;

    do {
        std::cout << "\n\nCurrent linked list data:\n";
        myLinkedList.PrintAllData();

        std::cout << "\nWhat would you like to do?\n"
            "1: Enter new node into linked list at head\n"
            "2: Enter new node into linked list at tail\n"
            "3: Enter new node into linked list at a certain position\n"
            "4: Delete node in linked list at head\n"
            "5: Delete node in linked list at tail\n"
            "6: Delete node in linked list at a certain position\n"
            "7: Change data at a certain position\n"
            "8: Get data at a certain position\n";                   
      
        //A note regarding cin:
        //When you read a number in from a stream, C++ only reads the number. Any additional whitespace and newlines are left in the buffer.
        //When you go to read in a string afterwrds, the next read operation will see the whitespace at the start of the buffer, 
        //consume it, and immediately return. You have to explicitly ignore the additional whitespace characters.


        option = getUserIntInput(option);

        switch (option) {
            case 1: //enter new node into linked list at head

                //TEST CASES:
                //1) No data: Tested JN 20210914
                //2) With data: Tested JN 20210914


                std::cout << "\nEnter in value to insert at head: ";
                dataToIns = getUserIntInput(dataToIns);

                res = myLinkedList.InsertAtHead(dataToIns);

                std::cout << "\nInserted " << res << " at head of linked list.\n";

                break; //need break after each case statement or else logic falls through

            case 2: //enter new node into linked list at tail

                //TEST CASES:
                //1) No data: Tested JN 20210914
                //2) With data: Tested JN 20210914

                std::cout << "\nEnter in value to insert at tail: ";
                dataToIns = getUserIntInput(dataToIns);

                res = myLinkedList.InsertAtTail(dataToIns);

                std::cout << "\nInserted " << res << " at tail of linked list.\n";

                break; //need break after each case statement or else logic falls through

            case 3: //enter new node into linked list at any position

                //TEST CASES:
                //1) Invalid position: Tested JN 20210914
                //2) First data in linked list: Tested JN 20210914
                //3) Insert into any position: Tested JN 20210914

                std::cout << "\nEnter in value to insert: ";
                dataToIns = getUserIntInput(dataToIns);
                std::cout << "\nEnter in position to insert at: ";
                pos = getUserIntInput(pos);

                res = myLinkedList.InsertAtPosition(dataToIns, pos);
                if (res == -1) {
                    std::cout << "\nPosition is not valid!\n";
                }
                else {
                    std::cout << "\nInserted " << res << " at position " << pos << " of linked list.\n";
                }

                break; //need break after each case statement or else logic falls through

            case 4:

                //TEST CASES:
                //1) More than one data in linked list: Tested JN 20210915
                //2) Only one data in linked list: Tested JN 20210915
                //3) Empty list: Tested JN 20210915

                std::cout << "\nDeleting node at head... ";
                res = myLinkedList.DeleteAtHead();
                if (res == -1) {
                    std::cout << "Delete unsuccessful!";
                }
                else {
                    std::cout << "Deleted head with value " << res;
                }

                break;
            case 5:

                //TEST CASES:
                //1) More than one data in linked list: Tested JN 20210915
                //2) Only one data in linked list: Tested JN 20210915
                //3) Empty list: Tested JN 20210915

                std::cout << "\nDeleting node at tail... ";
                res = myLinkedList.DeleteAtTail();
                if (res == -1) {
                    std::cout << "Delete unsuccessful!";
                }
                else {
                    std::cout << "Deleted tail with value " << res;
                }

                break;
            case 6:

                //TEST CASES:
                //1) More than one data in linked list: Tested JN 20210915
                //2) Only one data in linked list: Tested JN 20210915
                //3) Empty list: Tested JN 20210915
                //4) Invalid position: Tested JN 20210915

                std::cout << "\nEnter in position to delete at: ";
                pos = getUserIntInput(pos);
                std::cout << "\nDeleting node at position " << pos << "\n";
                res = myLinkedList.DeleteAtPosition(pos);
                if (res == -1) {
                    std::cout << "\nDelete unsuccessful!\n";
                }
                else {
                    std::cout << "\nDeleted mode at position " << pos << " with value " << res << "\n";
                }

                break;
            case 7:

                //TEST CASES:
                //1) Invalid position: Tested JN 2021/09/15
                //2) Data at any position: Tested JN 2021/09/15

                std::cout << "\nEnter in new value: ";
                dataToIns = getUserIntInput(dataToIns);
                std::cout << "\nEnter in position to set new value: ";
                pos = getUserIntInput(pos);

                res = myLinkedList.SetDataAtPosition(dataToIns, pos);
                if (res == -1) {
                    std::cout << "\nPosition is not valid!\n";
                }
                else {
                    std::cout << "\nSet " << res << " at position " << pos << " of linked list.\n";
                }

                break;
            case 8:

                //TEST CASES:
                //1) Invalid position: Tested JN 2021/09/15
                //2) Data at any position: Tested JN 2021/09/15

                std::cout << "\nEnter in position to get value: ";
                pos = getUserIntInput(pos);

                res = myLinkedList.GetDataAtPosition(pos);
                if (res == -1) {
                    std::cout << "\nPosition is not valid!\n";
                }
                else {
                    std::cout << "\nValue " << res << " is at position " << pos << " of linked list.\n";
                }

                break;

            default:
                std::cout << "No valid option selected. Please try again.\n";
        

        }
            

    } while (cont == true);


    

    std::cout << "Current Linked\n";

}




// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
