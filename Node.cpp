#include "Node.h"

//Constructor
Node::Node() {
	data = -1;
	next = nullptr; //set value to null
}

//Overloaded constructor
Node::Node(int initialData) {
	data = initialData;
	next = nullptr;
}

//Overloaded constructor
Node::Node(int initialData, Node* initialNext) {
	data = initialData;
	next = initialNext;
}

Node::~Node() {
	next = nullptr;
}