/* Linked List Class for holding nodes with positive integer values 
Author: Jian Lik Ng
Date: 2021/09/06
*/

#pragma once
#include "Node.h";

class LinkedList
{
	public:
		Node* head = nullptr; //ptr to the head
		Node* tail = nullptr;
		int length = 0;

		LinkedList();
		LinkedList(int initialData);
		~LinkedList();
		
		int InsertAtPosition(int data, int pos);
		int InsertAtHead(int data);
		int InsertAtTail(int data);
		int SetDataAtPosition(int data, int pos);
		int GetDataAtPosition(int pos);
		int GetDataAtHead();
		int GetDataAtTail();
		void PrintAllData();
		int DeleteAtPosition(int pos);
		int DeleteAtHead();
		int DeleteAtTail();

	private:
		Node* GetNodeAtPosition(int pos);
};

